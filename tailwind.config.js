module.exports = {
  theme: {
    extend: {
      colors: {
        'talenty-background': '#ECF1F6',
        primary: '#30308C',
        gray: '#80808A',
        secondary: '#80808A',
        'comment-box': '#ECF1F6',
        'soft-gray': '#cbcbcb',
      },
    },
  },
}
