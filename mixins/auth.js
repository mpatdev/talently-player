import { mapActions } from 'vuex'

export default {
  data() {
    return {
      loadingUser: true,
    }
  },
  async mounted() {
    if (
      this.$route.name === 'auth-login' ||
      this.$route.name === 'auth-register'
    ) {
      this.loadingUser = false
      return
    }
    try {
      this.loadingUser = true
      const isUserSigned = await this.checkUser()
      if (isUserSigned) {
        this.$router.push({ name: 'courses-slug' })
      }
    } catch (error) {
      if (this.$route.name !== 'auth-login') {
        this.$router.push({ name: 'auth-login' })
      }
    }
    this.loadingUser = false
  },
  methods: {
    ...mapActions('user', ['checkUser']),
  },
}
