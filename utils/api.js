import axios from 'axios'

axios.defaults.baseURL = process.env.API_URL

function setHeaders() {
  const bearerToken = localStorage.getItem('tlkey')
  if (bearerToken) {
    axios.defaults.headers.common.Authorization = `Bearer ${bearerToken}`
  }
}

export function removeAuthorization() {
  axios.defaults.headers.common.Authorization = ''
}

export function Get(url) {
  setHeaders()
  return axios.get(url).then(({ data }) => data)
}

export function Post(url, data) {
  setHeaders()
  return axios.post(url, data).then(({ data }) => data)
}

export function Put(url, data) {
  setHeaders()
  return axios.post(url, data).then(({ data }) => data)
}
export function Delete(url) {
  setHeaders()
  return axios.delete(url).then(({ data }) => data)
}

export default {
  Get,
  Post,
  Put,
  Delete,
}
