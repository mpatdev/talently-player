# Talently Frontend Challenge

## Instrucciones de Nuxt
```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

## Dependencias

- [NuxtJs](https://nuxtjs.org/)
- [Tailwind css](https://tailwindcss.com/)
- [Nuxt Tailwind](https://tailwindcss.com/docs/guides/nuxtjs)
- [Axios](https://github.com/axios/axios)

