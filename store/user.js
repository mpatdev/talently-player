import { Post, removeAuthorization } from '~/utils/api'

export const state = () => {
  return {
    user: null,
    accessToken: null,
  }
}

export const actions = {
  login(_, payload) {
    return Post('auth/login', payload).then((user) => {
      localStorage.setItem('tlkey', user.access_token)
      return user
    })
  },
  register(_, payload) {
    return Post('auth/register', payload).then((user) => {
      console.log(user)
      if (user.success) {
        return user.message
      } else {
        return null
      }
    })
  },
  logout() {
    localStorage.removeItem('tlkey')
    removeAuthorization()
    this.$router.push({ name: 'auth-login' })
  },
  checkUser({ commit }) {
    return Post('auth/me').then((data) => {
      commit('SET_USER', data)
      return data
    })
  },
}

export const mutations = {
  SET_USER(state, payload) {
    state.user = payload
  },
}
