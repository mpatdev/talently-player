import { Delete, Post } from '~/utils/api'

export const state = () => ({
  contentList: [],
  currentContent: null,
  comments: [],
})

export const actions = {
  deleteComment({ dispatch, state }, commentid) {
    return Delete(`comment/${commentid}`).then((data) => {
      if (data.success === false) {
        return false
      }
      dispatch('removeComment', commentid)
      return data
    })
  },
  updateComment({ commit }, payload) {
    return Post(`comment/${payload.comment_id}/update`, {
      content: payload.content,
    }).then((data) => {
      commit('SET_CONTENT_COMMENT', payload)
    })
  },
  removeComment({ commit, state }, commentid) {
    const indexOfComment = state.comments.findIndex((x) => x.id === commentid)

    commit('REMOVE_COMMENT', indexOfComment)
  },
  createComment({ commit, dispatch }, comment) {
    return Post(`comment/create`, comment).then((data) => {
      if (data.success === false) {
        return null
      }
      dispatch('getComments', comment.content_id)
      return data
    })
  },
  setContentAsCurrent({ commit, dispatch }, content) {
    commit('SET_CURRENT', content)
    dispatch('setRouteContent', content.id)
    dispatch('getComments', content.id)
  },
  setRouteContent(_, contentid) {
    this.$router.push(`/courses/talently-tech-challenge/${contentid}`)
  },
  async endContent({ commit, dispatch, state }) {
    await dispatch('updateProgress', {
      contentid: state.currentContent.id,
      progress: 100,
    })

    dispatch('setNextContent')
  },
  setNextContent({ dispatch, commit, state }) {
    const { contentList, currentContent } = state
    const currentIndex = contentList.findIndex(
      (x) => x.id === currentContent.id
    )

    if (currentIndex < contentList.length - 1) {
      const content = contentList[currentIndex + 1]
      dispatch('setContentAsCurrent', content)
    }
  },
  getComments({ commit }, contentid) {
    return Post(`content/${contentid}/comments`).then(({ comments }) => {
      commit('SET_COMMENTS', comments)
    })
  },
  updateProgress({ commit }, { contentid, progress }) {
    return Post(`content/${contentid}/progress`, { progress }).then(() => {
      commit('SET_PROGRESS', progress)
    })
  },
  setProgress({ commit }, payload) {
    commit('SET_PROGRESS', payload)
  },
  getContent({ commit, dispatch }) {
    return Post('content').then((data) => {
      const { content } = data
      const contentNotEndend = content.find((x) => x.progress < 100)
      if (contentNotEndend) {
        dispatch('setContentAsCurrent', contentNotEndend)
      } else {
        dispatch('setContentAsCurrent', content[0])
      }

      commit('SET_CONTENT', content)

      return data.content
    })
  },
}

export const mutations = {
  SET_CONTENT_COMMENT(state, payload) {
    const comment = state.comments.find((x) => x.id === payload.comment_id)
    if (comment) {
      comment.content = payload.content
    }
  },
  REMOVE_COMMENT(state, payload) {
    state.comments = state.comments.filter((_, index) => index !== payload)
  },
  SET_COMMENTS(state, payload) {
    state.comments = payload
  },
  SET_PROGRESS(state, payload) {
    const currentItem = state.contentList.find(
      (x) => x.id === state.currentContent.id
    )
    if (currentItem.progress < payload) {
      currentItem.progress = payload
    }
  },
  SET_CONTENT(state, payload) {
    state.contentList = payload
  },
  SET_CURRENT(state, payload) {
    state.currentContent = payload
  },
}
